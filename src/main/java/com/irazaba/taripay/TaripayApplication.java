package com.irazaba.taripay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TaripayApplication {

	public static void main(String[] args) {
		SpringApplication.run(TaripayApplication.class, args);
	}

}
