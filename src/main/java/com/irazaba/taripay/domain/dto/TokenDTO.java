package com.irazaba.taripay.domain.dto;

public record TokenDTO(String token) {
}
