package com.irazaba.taripay.domain.dto;

public record LogInDTO(String email,
                       String password) {
}
