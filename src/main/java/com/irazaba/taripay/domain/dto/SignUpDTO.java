package com.irazaba.taripay.domain.dto;

public record SignUpDTO(String name,
                        String lastName,
                        String email,
                        String password) {
}
