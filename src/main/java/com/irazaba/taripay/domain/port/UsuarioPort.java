package com.irazaba.taripay.domain.port;

import com.irazaba.taripay.domain.dto.LogInDTO;
import com.irazaba.taripay.domain.dto.SignUpDTO;
import com.irazaba.taripay.domain.dto.TokenDTO;
import com.irazaba.taripay.domain.model.Usuario;
import reactor.core.publisher.Mono;

public interface UsuarioPort {
    Mono<Usuario> signUp(SignUpDTO dto);
    Mono<TokenDTO> login(LogInDTO dto);
}
