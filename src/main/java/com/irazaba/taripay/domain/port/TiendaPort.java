package com.irazaba.taripay.domain.port;

import com.irazaba.taripay.domain.model.Tienda;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface TiendaPort {

    Mono<Tienda> save(Tienda model);
    Flux<Tienda> findByUser(Long usuarioId);
}
