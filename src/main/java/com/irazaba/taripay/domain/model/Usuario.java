package com.irazaba.taripay.domain.model;

public record Usuario(Long id,
                      String name,
                      String lastname,
                      String email,
                      String password,
                      Boolean status,
                      String roles) {
}
