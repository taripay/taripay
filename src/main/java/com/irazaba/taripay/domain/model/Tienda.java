package com.irazaba.taripay.domain.model;

import java.time.LocalDate;

public record Tienda(Long id,
         String nombre,
         String categoria,
         String lat,
         String lng,
         LocalDate fechaAlta,
         String urlImagen,
         Long usuarioId) {
}
