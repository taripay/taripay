package com.irazaba.taripay.app.service;

import com.irazaba.taripay.app.usecase.TiendaUseCase;
import com.irazaba.taripay.domain.model.Tienda;
import com.irazaba.taripay.domain.port.TiendaPort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class TiendaService implements TiendaUseCase {

    private final TiendaPort tiendaPort;

    @Override
    public Mono<Tienda> save(Tienda model) {
        return tiendaPort.save(model);
    }

    @Override
    public Flux<Tienda> findByUsuario(Long usuarioId) {
        return tiendaPort.findByUser(usuarioId);
    }
}
