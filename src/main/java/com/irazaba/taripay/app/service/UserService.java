package com.irazaba.taripay.app.service;

import com.irazaba.taripay.app.usecase.LogInUseCase;
import com.irazaba.taripay.app.usecase.SignUpUseCase;
import com.irazaba.taripay.domain.dto.LogInDTO;
import com.irazaba.taripay.domain.dto.SignUpDTO;
import com.irazaba.taripay.domain.dto.TokenDTO;
import com.irazaba.taripay.domain.model.Usuario;
import com.irazaba.taripay.domain.port.UsuarioPort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class UserService implements LogInUseCase, SignUpUseCase {

    private final UsuarioPort usuarioPort;

    @Override
    public Mono<TokenDTO> login(LogInDTO dto) {
        return this.usuarioPort.login(dto);
    }

    @Override
    public Mono<Usuario> signUp(SignUpDTO dto) {
        return this.usuarioPort.signUp(dto);
    }
}
