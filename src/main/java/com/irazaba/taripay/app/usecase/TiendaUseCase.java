package com.irazaba.taripay.app.usecase;

import com.irazaba.taripay.domain.model.Tienda;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface TiendaUseCase {
    Mono<Tienda> save(Tienda model);
    Flux<Tienda> findByUsuario(Long usuarioId);
}
