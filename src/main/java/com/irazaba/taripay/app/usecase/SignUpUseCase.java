package com.irazaba.taripay.app.usecase;

import com.irazaba.taripay.domain.dto.SignUpDTO;
import com.irazaba.taripay.domain.model.Usuario;
import reactor.core.publisher.Mono;

public interface SignUpUseCase {

    Mono<Usuario> signUp(SignUpDTO dto);
}
