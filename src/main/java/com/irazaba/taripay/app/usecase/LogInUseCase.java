package com.irazaba.taripay.app.usecase;

import com.irazaba.taripay.domain.dto.LogInDTO;
import com.irazaba.taripay.domain.dto.SignUpDTO;
import com.irazaba.taripay.domain.dto.TokenDTO;
import com.irazaba.taripay.domain.model.Usuario;
import reactor.core.publisher.Mono;

public interface LogInUseCase {

    Mono<TokenDTO> login(LogInDTO dto);
}
