package com.irazaba.taripay.infraestructure.entrypoint.reactiveweb.handler;

import com.irazaba.taripay.app.service.TiendaService;
import com.irazaba.taripay.domain.model.Tienda;
import lombok.RequiredArgsConstructor;
import net.sf.jsqlparser.schema.Server;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;


@Component
@RequiredArgsConstructor
public class TiendaHandler {

    private final TiendaService tiendaService;

    public Mono<ServerResponse> save(ServerRequest request){
        return request.bodyToMono(Tienda.class)
                .flatMap( model -> ServerResponse.ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(tiendaService.save(model), Tienda.class)
                );
    }

    public Mono<ServerResponse> findByUsuario(ServerRequest request){
        Long usuarioId = Long.parseLong(request.pathVariable("usuarioId"));
        return ServerResponse.ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(tiendaService.findByUsuario(usuarioId), Tienda.class);
    }

}
