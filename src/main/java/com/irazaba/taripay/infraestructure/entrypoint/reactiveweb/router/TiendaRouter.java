package com.irazaba.taripay.infraestructure.entrypoint.reactiveweb.router;
import com.irazaba.taripay.infraestructure.entrypoint.reactiveweb.handler.TiendaHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;


@Configuration
public class TiendaRouter {

    private static final String PATH = "tienda";

    @Bean
    public RouterFunction<ServerResponse> routerTienda(TiendaHandler handler){
        return RouterFunctions.route()
                .GET(PATH + "/{usuarioId}", handler::findByUsuario)
                .POST(PATH, handler::save)
                .build();
    }
}
