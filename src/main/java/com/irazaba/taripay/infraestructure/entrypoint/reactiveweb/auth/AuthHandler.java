package com.irazaba.taripay.infraestructure.entrypoint.reactiveweb.auth;

import com.irazaba.taripay.app.service.UserService;
import com.irazaba.taripay.app.usecase.SignUpUseCase;
import com.irazaba.taripay.domain.dto.LogInDTO;
import com.irazaba.taripay.domain.dto.SignUpDTO;
import com.irazaba.taripay.domain.dto.TokenDTO;
import com.irazaba.taripay.domain.model.Usuario;
import lombok.RequiredArgsConstructor;
import net.sf.jsqlparser.schema.Server;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

@Component
@RequiredArgsConstructor
public class AuthHandler {

    private final UserService userService;

    public Mono<ServerResponse> signUp(ServerRequest request){
        return request.bodyToMono(SignUpDTO.class)
                .flatMap(dto -> ServerResponse.ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(userService.signUp(dto), Usuario.class)
                );
    }

    public Mono<ServerResponse> login(ServerRequest request){
       return request.bodyToMono(LogInDTO.class)
               .flatMap( dto -> ServerResponse.ok()
                       .contentType(MediaType.APPLICATION_JSON)
                       .body(userService.login(dto), TokenDTO.class)
               );
    }

    public Mono<ServerResponse> hello(ServerRequest request){
        return ServerResponse.ok()
                .contentType(MediaType.TEXT_PLAIN)
                .body(Mono.just("hello"), String.class);
    }
}
