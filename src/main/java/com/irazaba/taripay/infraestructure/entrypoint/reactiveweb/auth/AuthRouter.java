package com.irazaba.taripay.infraestructure.entrypoint.reactiveweb.auth;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

@Configuration
public class AuthRouter {

    private static final String PATH = "auth/";

    @Bean
    public RouterFunction<ServerResponse> routerAuth(AuthHandler handler){
        return RouterFunctions.route()
                .GET("hello", handler::hello)
                .POST(PATH + "signup", handler::signUp)
                .POST(PATH + "login", handler::login)
                .build();

    }
}
