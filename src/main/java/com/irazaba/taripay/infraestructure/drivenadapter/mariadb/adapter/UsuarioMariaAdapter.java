package com.irazaba.taripay.infraestructure.drivenadapter.mariadb.adapter;

import com.irazaba.taripay.domain.dto.LogInDTO;
import com.irazaba.taripay.domain.dto.SignUpDTO;
import com.irazaba.taripay.domain.dto.TokenDTO;
import com.irazaba.taripay.domain.model.Usuario;
import com.irazaba.taripay.domain.port.UsuarioPort;
import com.irazaba.taripay.infraestructure.drivenadapter.mariadb.entity.UsuarioEntity;
import com.irazaba.taripay.infraestructure.drivenadapter.mariadb.mapper.UsusarioMapper;
import com.irazaba.taripay.infraestructure.drivenadapter.mariadb.repository.UsuarioReactiveMariaRepository;
import com.irazaba.taripay.infraestructure.drivenadapter.security.jwt.provider.JwtProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
@RequiredArgsConstructor
public class UsuarioMariaAdapter implements UsuarioPort {

    private final UsuarioReactiveMariaRepository repository;
    private final PasswordEncoder passwordEncoder;
    private final JwtProvider jwtProvider;

    @Override
    public Mono<Usuario> signUp(SignUpDTO dto) {
        var usuarioEntity = getUsuarioEntity(dto);
        return repository.save(usuarioEntity)
                .map(UsusarioMapper::mapToModel);
    }

    @Override
    public Mono<TokenDTO> login(LogInDTO dto) {
        return repository.findByEmail(dto.email())
                .filter(entity -> passwordEncoder.matches(dto.password(), entity.getPassword()))
                .map(entity -> new TokenDTO(jwtProvider.generateToken(entity)))
                .switchIfEmpty(Mono.error(new Throwable("bad credentials")));
    }

    private UsuarioEntity getUsuarioEntity(SignUpDTO dto){
        return UsuarioEntity.builder()
                .name(dto.name())
                .lastname(dto.lastName())
                .email(dto.email())
                .password(passwordEncoder.encode(dto.password()))
                .status(true)
                .build();
    }
}
