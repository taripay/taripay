package com.irazaba.taripay.infraestructure.drivenadapter.mariadb.repository;

import com.irazaba.taripay.infraestructure.drivenadapter.mariadb.entity.TiendaEntity;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface TiendaReactiveMariaRepository extends ReactiveCrudRepository<TiendaEntity, Long> {

    Flux<TiendaEntity> findByUsuarioId(Long usuarioId);
}
