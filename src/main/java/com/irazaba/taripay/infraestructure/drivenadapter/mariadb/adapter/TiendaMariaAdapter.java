package com.irazaba.taripay.infraestructure.drivenadapter.mariadb.adapter;

import com.irazaba.taripay.domain.model.Tienda;

import com.irazaba.taripay.domain.port.TiendaPort;

import com.irazaba.taripay.infraestructure.drivenadapter.mariadb.mapper.TiendaMapper;
import com.irazaba.taripay.infraestructure.drivenadapter.mariadb.repository.TiendaReactiveMariaRepository;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
@RequiredArgsConstructor
public class TiendaMariaAdapter implements TiendaPort {

    private final TiendaReactiveMariaRepository repository;


    @Override
    public Mono<Tienda> save(Tienda model) {
        return repository.save(TiendaMapper.mapToEntity(model))
                .map(TiendaMapper::mapToModel);

    }

    @Override
    public Flux<Tienda> findByUser(Long usuarioId) {
        return repository.findByUsuarioId(usuarioId)
                .map(TiendaMapper::mapToModel);
    }
}
