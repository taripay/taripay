package com.irazaba.taripay.infraestructure.drivenadapter.mariadb.mapper;

import com.irazaba.taripay.domain.model.Tienda;
import com.irazaba.taripay.infraestructure.drivenadapter.mariadb.entity.TiendaEntity;

public class TiendaMapper {

    public static Tienda mapToModel(TiendaEntity entity){
        return new Tienda(
                entity.getId(),
                entity.getNombre(),
                entity.getCategoria(),
                entity.getLat(),
                entity.getLng(),
                entity.getFechaAlta(),
                entity.getUrlImagen(),
                entity.getUsuarioId());
    }

    public static TiendaEntity mapToEntity(Tienda model){
        return TiendaEntity.builder()
                .id(model.id())
                .nombre(model.nombre())
                .categoria(model.categoria())
                .lat(model.lat())
                .lng(model.lng())
                .fechaAlta(model.fechaAlta())
                .urlImagen(model.urlImagen())
                .usuarioId(model.usuarioId())
                .build();
    }
}
