package com.irazaba.taripay.infraestructure.drivenadapter.mariadb;

import io.r2dbc.spi.ConnectionFactories;
import io.r2dbc.spi.ConnectionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.r2dbc.config.AbstractR2dbcConfiguration;

@Configuration
public class DBConfig extends AbstractR2dbcConfiguration {

    @Value("${spring.r2dbc.url")
    private String dbUrl;
    @Override
    public ConnectionFactory connectionFactory() {
        return ConnectionFactories.get(dbUrl);
    }
}
