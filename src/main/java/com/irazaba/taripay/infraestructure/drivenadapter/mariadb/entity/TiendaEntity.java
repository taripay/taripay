package com.irazaba.taripay.infraestructure.drivenadapter.mariadb.entity;

import lombok.*;
import org.springframework.data.relational.core.mapping.Table;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(value="tb_tienda")
public class TiendaEntity {

    private Long id;
    private String nombre;
    private String categoria;
    private String lat;
    private String lng;
    private LocalDate fechaAlta;
    private String urlImagen;
    private Long usuarioId;
}
