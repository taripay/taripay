package com.irazaba.taripay.infraestructure.drivenadapter.mariadb.mapper;

import com.irazaba.taripay.domain.model.Usuario;
import com.irazaba.taripay.infraestructure.drivenadapter.mariadb.entity.UsuarioEntity;

public class UsusarioMapper {

    public static Usuario mapToModel(UsuarioEntity entity){
        return new Usuario(
                entity.getId(),
                entity.getName(),
                entity.getLastname(),
                entity.getEmail(),
                entity.getPassword(),
                entity.isStatus(),
                entity.getRoles());
    }

    public static UsuarioEntity mapToEntity(Usuario model){
        return UsuarioEntity.builder()
                .id(model.id())
                .name(model.name())
                .lastname(model.lastname())
                .email(model.email())
                .password(model.password())
                .status(model.status())
                .roles(model.roles())
                .build();
    }
}
