package com.irazaba.taripay.infraestructure.drivenadapter.mariadb.repository;

import com.irazaba.taripay.infraestructure.drivenadapter.mariadb.entity.UsuarioEntity;
import org.springframework.context.annotation.Bean;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

public interface UsuarioReactiveMariaRepository extends ReactiveCrudRepository<UsuarioEntity, Long> {

    Mono<UsuarioEntity> findByEmail(String email);
}
